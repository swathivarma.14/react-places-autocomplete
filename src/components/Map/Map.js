import React, { useState, useRef, useEffect } from 'react';
import { GoogleMap, MarkerF, Autocomplete, DirectionsRenderer } from '@react-google-maps/api';
import './Map.css';
import Box from '@mui/material/Box';
import { Input } from '@mui/material';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { InputLabel } from '@mui/material';
import NearMeIcon from '@mui/icons-material/NearMe';
import { addSearchedData, getSearchList } from '../../redux/actions/SearchAction';
import { connect } from 'react-redux';

const center = {lat: 3.0644611811684723, lng: 101.66423695530099}

function Map({getSearchList, search, addSearchedData}) {

    const [map, setMap] = useState( /** @type google.maps.Map */ (null));
    const [directionsResponse, setDirectionsResponse] = useState(null);
    const [duration, setDuration] = useState('');
    const [distance, setDistance] = useState('');

    /** @type React.MutableRefObject<HTMLInputElement> */
    const originRef = useRef('')

    /** @type React.MutableRefObject<HTMLInputElement> */
    const destinationRef = useRef('');

    useEffect(() => {
        getSearchList();
    });

    const calculateRoute = async() => {
        if (originRef.current.value === '' || destinationRef.current.value === '') {
            return
        }
        // eslint-disable-next-line no-undef
        const directionsService = new google.maps.DirectionsService();
        const results = await directionsService.route({
            origin: originRef.current.value,
            destination: destinationRef.current.value,
            // eslint-disable-next-line no-undef
            travelMode: google.maps.TravelMode.DRIVING,
        })
        setDirectionsResponse(results);
        setDistance(results.routes[0].legs[0].distance.text);
        setDuration(results.routes[0].legs[0].duration.text);
    }

    const clearRoute = () => {
        setDirectionsResponse(null);
        setDistance('');
        setDuration('');
        originRef.current.value = '';
        destinationRef.current.value = '';
    }

    return (
        <>
            <Box className='box'>
                <React.Fragment>
                    <Grid item xs={3} className="item">
                        <Autocomplete>
                            <Input
                                style={{ padding: '5px' }}
                                placeholder="Origin"
                                type="text"
                                inputRef={originRef}
                            />
                        </Autocomplete>
                    </Grid>
                    <Grid item xs={3} className="item">
                        <Autocomplete>
                            <Input
                                style={{ padding: '5px' }}
                                placeholder="Destination"
                                type="text"
                                inputRef={destinationRef}
                            />
                        </Autocomplete>
                    </Grid>
                    <Grid item xs={3} className="button">
                        <Button onClick={calculateRoute} variant="contained">Calculate Route</Button>
                    </Grid>
                    <Grid item xs={3} className="button">
                        <Button onClick={clearRoute} variant="contained">
                            Clear Route &nbsp;
                        </Button>
                    </Grid>
                    <Grid item xs={3} className="button">
                        <Button onClick={() => addSearchedData( {origin: originRef.current.value, destination: destinationRef.current.value})} variant="contained">
                            Show Search Results
                        </Button>
                    </Grid>
                </React.Fragment>
            </Box>
            <Box className="box" m={1} p={1}>
                <React.Fragment>
                    <Grid item xs={4} className="item">
                        <InputLabel>Distance: {distance}</InputLabel>
                    </Grid>
                    <Grid item xs={4} className="item">
                        <InputLabel>Duration: {duration}</InputLabel>
                    </Grid>
                    <Grid className="button">
                        <Button onClick={ () => map.panTo(center) } variant="contained">
                            Re-Center &nbsp; <NearMeIcon className='size' />
                        </Button>
                    </Grid>
                    <Grid>
                    {search.map((search, index) => (
                        <div className='search-result' key={index}>
                                {search.origin}
                        </div>
                    ))}
                    </Grid>
                </React.Fragment>
            </Box>
            <GoogleMap
                zoom={10}
                center={center}
                mapContainerClassName='map-container'
                onLoad={(map) => setMap(map)}
            >
                <MarkerF position={center}/>
                {directionsResponse && 
                    <DirectionsRenderer directions={directionsResponse}/>
                }
            </GoogleMap>
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        search: state.search,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getSearchList: () => dispatch(getSearchList()),
        addSearchedData: ({origin, destination}) => dispatch(addSearchedData({origin, destination}))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Map);
