export const getSearchList = () => {
    return {type: "GET_LIST"}
};

export const addSearchedData = (searchObj) => {
    return {type: "ADD_SEARCH", payload: searchObj}
};