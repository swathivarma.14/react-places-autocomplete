const initial = {search: [{origin: "", destination: ""}]};

export const SearchReducer = (state = initial, action) => {

  switch (action.type) {
    case "GET_LIST":
        return {...state}
    case "ADD_SEARCH":{
      let search = [...state.search];
      search.push(action.payload)
      return {...state, search: search}
    }
    default:
        return state;
  }
}
