import { configureStore } from "@reduxjs/toolkit";
import { SearchReducer } from "./reducers/SearchReducer";

export const store = configureStore({reducer: SearchReducer});
